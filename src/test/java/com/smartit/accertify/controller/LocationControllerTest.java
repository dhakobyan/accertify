package com.smartit.accertify.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartit.accertify.domain.Location;
import com.smartit.accertify.service.LocationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(LocationController.class)
class LocationControllerTest {

    Location location;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LocationService locationService;

    @BeforeEach
    void setUp() {
        Assertions.assertNotNull(mockMvc);
        Assertions.assertNotNull(locationService);
        location = new Location(33.66946, -117.82311, "Irvine", "California", "US");
    }

    @AfterEach
    void tearDown() {
        mockMvc = null;
        locationService = null;
    }

    @Test
    void getGeoLocation_returnLocation() throws Exception {
        // given
        given(locationService.getData(location.getLat(), location.getLng())).willReturn(location);

        // when
        String controllerUrl = String.format("/%s/%s", location.getLat(), location.getLng());
        ResultActions modelName = mockMvc.perform(get(controllerUrl))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        modelName.andExpect(jsonPath("lat").value(location.getLat()));
        modelName.andExpect(jsonPath("lng").value(location.getLng()));
        modelName.andExpect(jsonPath("city").value(location.getCity()));
    }

    @Test
    void getGeoLocation_throw_exception() throws Exception {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");

        // when
        String controllerUrl = String.format("/%s/%s", location.getLat(), location.getLng());
        mockMvc.perform(get(controllerUrl))
                .andDo(print())
                .andExpect(status().is4xxClientError());

        // then
    }


//    @Test
    void validateActivityLocation_return_withinUSA_true() throws Exception {
        // given
        given(locationService.isWithinUSA(location)).willReturn(true);

        // when
        ResultActions modelName = mockMvc.perform(post("/validateActivityLocation")
                .content(asJsonString(location))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        modelName.andExpect(jsonPath("withinUSA").value(true));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}