package com.smartit.accertify.service;

import com.smartit.accertify.domain.Constants;
import com.smartit.accertify.domain.Location;
import com.smartit.accertify.exception.DataNotFoundException;
import com.smartit.accertify.repository.LocationRepository;
import com.smartit.accertify.unti.DistanceUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class LocationServiceTest {

    @Autowired
    LocationService locationService;

    @Mock
    private LocationRepository locationRepository;

    @Before
    public void setUp() throws Exception {
        assertNotNull(locationRepository);
        locationService = new LocationService(locationRepository);
        assertNotNull(locationService);
    }

    @Test
    public void withinUSA_returnTrue() {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");
        given(locationRepository.findByLatAndLng(location.getLat(), location.getLng())).willReturn(location);

        // when
        boolean withinUSA = locationService.isWithinUSA(location);

        // then
        assertTrue(withinUSA);
    }

    @Test
    public void withinUSA_returnFalse() {
        // given
        Location location = DistanceUtil.getCityLocation(Constants.Tokyo_Japan);
        given(locationRepository.findByLatAndLng(location.getLat(), location.getLng())).willReturn(location);

        // when
        boolean withinUSA = locationService.isWithinUSA(location);

        // then
        assertFalse(withinUSA);
    }

    @Test
    public void within500MilesOfCity_returnTrue() {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");

        // when
        boolean within500KmOfCity = locationService.within500MilesOfCity(location, Constants.Westminster_CA);

        // then
        assertTrue(within500KmOfCity);
    }

    @Test
    public void within500MilesOfCity_returnFalse() {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");

        // when
        boolean within500KmOfCity = locationService.within500MilesOfCity(location, Constants.Tokyo_Japan);

        // then
        assertFalse(within500KmOfCity);
    }

    @Test
    public void distanceInMiles_returnCityLocation() {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");

        // when
        double distanceInMiles = 0;
        try {
            distanceInMiles = locationService.distanceInMiles(location, Constants.Mexico_City_Mexico);
        } catch (DataNotFoundException e) {
            fail(e.getMessage());
        }

        // then
        assertTrue(distanceInMiles > 0);
    }

    @Test
    public void distanceInMiles_throw_DataNotFoundException() {
        // given
        Location location = new Location(33.66946, -117.82311, "Irvine", "California", "US");
        String city = "yerevan";

        // when
        double distanceInMiles = 0;
        try {
            distanceInMiles = locationService.distanceInMiles(location, city);
        } catch (DataNotFoundException e) {
            assertEquals("Unknown city: " + city, e.getMessage());
        }

        // then
        assertEquals(0, distanceInMiles);
    }
}