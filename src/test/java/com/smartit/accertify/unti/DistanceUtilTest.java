package com.smartit.accertify.unti;

import com.smartit.accertify.domain.Constants;
import com.smartit.accertify.domain.Location;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistanceUtilTest {

    @Test
    void calculateDistanceInMeters() {
        Float distance = 7820109.5f;
        Location location1 = DistanceUtil.getCityLocation(Constants.Tokyo_Japan);
        Location location2 = DistanceUtil.getCityLocation(Constants.Sydney_Australia);
        double dist = DistanceUtil.calculateDistanceInMeters(location1, location2);
        assertTrue(dist > 0);
    }

    @Test
    void meterToMile() {
        assertEquals(1, DistanceUtil.meterToMile(DistanceUtil.MILE_IN_METERS));
    }
}