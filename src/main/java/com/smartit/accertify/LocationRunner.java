package com.smartit.accertify;

import com.smartit.accertify.domain.Location;
import com.smartit.accertify.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class LocationRunner implements CommandLineRunner {

    private final LocationRepository locationRepository;

    @Autowired
    public LocationRunner(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    boolean doNotPopulateDB = true;

    @Override
    public void run(String... args) throws Exception {
        if (doNotPopulateDB)
            return;

        System.out.println("start populating the database");
        Path path = Paths.get("data/rg_cities1000-6.csv");
        List<Location> locations = new ArrayList<>();
        try (InputStream is = Files.newInputStream(path);
             BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line = null;
            boolean isFirstLine = true;
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue;
                }
                // lat | lon | name | admin1 | cc
                // lat | lng | city | state | country

                String[] chunks = line.split(",");
                if ("Irvine".equals(chunks[2])) {
                    System.out.println("stop");
                }
                Location location = new Location(Double.parseDouble(chunks[0]), Double.parseDouble(chunks[1]),
                        chunks[2], chunks[3], chunks[5]);
                locations.add(location);

                if (locations.size() == 1000) {
                    locationRepository.saveAll(locations);
                    locations.clear();
                }
            }
            if (locations.size() > 0) {
                locationRepository.saveAll(locations);
                locations.clear();
            }
        } catch (IOException x) {
            x.printStackTrace();
            System.exit(0);
        }
    }
}
