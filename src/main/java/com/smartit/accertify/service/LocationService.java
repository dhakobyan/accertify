package com.smartit.accertify.service;

import com.smartit.accertify.domain.Constants;
import com.smartit.accertify.domain.Activity;
import com.smartit.accertify.domain.Location;
import com.smartit.accertify.repository.LocationRepository;
import com.smartit.accertify.unti.DistanceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class LocationService {

    private final LocationRepository locationRepository;

    @Autowired
    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    //    a) getAllDataSets() - GET method, returns all data in the DB
    public List<Location> getAllDataSets() {
        return locationRepository.findAll();
    }

    //    b) getData(latitude, longitude) - GET method, returns if the coordinates exist in the DB or not
    public Location getData(Double latitude, Double longitude) {
//        return new GeoLocation(latitude, longitude);
        return locationRepository.findByLatAndLng(latitude, longitude);
    }

    //    c) addData(latitude, longitude) - POST method, adds the coordinate to the DB if it doesn't exist
    public void addData(Double latitude, Double longitude) {
        locationRepository.save(new Location(latitude, longitude));
    }

    public boolean locationExists(Double latitude, Double longitude) {
        return locationRepository.existsByLatAndLng(latitude, longitude);
    }


    /**
     * Checik if location withing the USA
     *
     * @param location Location
     * @return boolean
     */
    public boolean isWithinUSA(Location location) {
        Location result = locationRepository.findByLatAndLng(location.getLat(), location.getLng());
        return Constants.USA.equalsIgnoreCase(result.getCountry());
    }

    public boolean isWithinUSA(Double latitude, Double longitude) {
        Location result = locationRepository.findByLatAndLng(latitude, longitude);
        return Constants.USA.equalsIgnoreCase(result.getCountry());
    }

    /**
     * check if location is within 500 miles from the city
     *
     * @param location Location
     * @param city     String
     * @return boolean
     */
    public boolean within500MilesOfCity(Location location, String city) {
        return 500 > distanceInMiles(location, city);
    }

    /**
     * calculate the distance between the location and the city
     *
     * @param location Location
     * @param city     String
     * @return Double
     */
    public Double distanceInMiles(Location location, String city) {
        Location cityLocation = DistanceUtil.getCityLocation(city);
        double distanceInMeters = DistanceUtil.calculateDistanceInMeters(location, cityLocation);
        return DistanceUtil.meterToMile(distanceInMeters);
    }

    /**
     * returl the list of cities, with the distance of activity and whether activity within 500 miles
     * @param location Location
     * @return List<Activity>
     */
    public List<Activity> fraudGeoCharacteristics(Location location) {
        Double distanceInMiles1 = distanceInMiles(location, Constants.Tokyo_Japan);
        Double distanceInMiles2 = distanceInMiles(location, Constants.Sydney_Australia);
        Double distanceInMiles3 = distanceInMiles(location, Constants.Riyadh_Saudi_Arabia);
        Double distanceInMiles4 = distanceInMiles(location, Constants.Zurich_Switzerland);
        Double distanceInMiles5 = distanceInMiles(location, Constants.Reykjavik_Iceland);
        Double distanceInMiles6 = distanceInMiles(location, Constants.Mexico_City_Mexico);
        Double distanceInMiles7 = distanceInMiles(location, Constants.Lima_Peru);

        return Arrays.asList(new Activity(Constants.Tokyo_Japan, distanceInMiles1),
                new Activity(Constants.Sydney_Australia, distanceInMiles2),
                new Activity(Constants.Riyadh_Saudi_Arabia, distanceInMiles3),
                new Activity(Constants.Zurich_Switzerland, distanceInMiles4),
                new Activity(Constants.Reykjavik_Iceland, distanceInMiles5),
                new Activity(Constants.Mexico_City_Mexico, distanceInMiles6),
                new Activity(Constants.Lima_Peru, distanceInMiles7));
    }
}
