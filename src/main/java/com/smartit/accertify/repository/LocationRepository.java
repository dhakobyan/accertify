package com.smartit.accertify.repository;

import com.smartit.accertify.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    Location findByLatAndLng(Double latitude, Double longitude);

    boolean existsByLatAndLng(Double latitude, Double longitude);
}
