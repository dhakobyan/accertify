package com.smartit.accertify.unti;

import com.smartit.accertify.domain.Constants;
import com.smartit.accertify.domain.Location;
import com.smartit.accertify.exception.DataNotFoundException;
import org.apache.lucene.util.SloppyMath;

public class DistanceUtil {

    public static final double MILE_IN_METERS = 1609.344;

    public static double calculateDistanceInMeters(Location location1, Location location2) {
        return SloppyMath.haversinMeters(location1.getLat(), location1.getLng(), location2.getLat(), location2.getLng());
    }

    public static Location getCityLocation(String city) {
        if (Constants.Tokyo_Japan.equalsIgnoreCase(city))
            return new Location(35.652832, 139.839478);
        if (Constants.Sydney_Australia.equalsIgnoreCase(city))
            return new Location(-33.866867, 151.212845);
        if (Constants.Riyadh_Saudi_Arabia.equalsIgnoreCase(city))
            return new Location(24.774265, 46.738586);
        if (Constants.Zurich_Switzerland.equalsIgnoreCase(city))
            return new Location(47.373878, 8.545094);
        if (Constants.Reykjavik_Iceland.equalsIgnoreCase(city))
            return new Location(64.128288, -21.827774);
        if (Constants.Mexico_City_Mexico.equalsIgnoreCase(city))
            return new Location(19.432608, -99.133209);
        if (Constants.Lima_Peru.equalsIgnoreCase(city))
            return new Location(-12.046374, -77.042793);
        if (Constants.Westminster_CA.equalsIgnoreCase(city))
            return new Location(33.754078486410705, -118.01721458836349);
        throw new DataNotFoundException("Unknown city: " + city);
    }

    public static Double meterToMile(Double distanceInMeters) {
        return (distanceInMeters / MILE_IN_METERS);
    }
}
