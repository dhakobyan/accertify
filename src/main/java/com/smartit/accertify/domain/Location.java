package com.smartit.accertify.domain;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Configurable;
import org.apache.lucene.util.SloppyMath;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Configurable
@Table(name = "location")
public class Location implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;

    @NotNull
    private String city;

    @NotNull
    private String state;

    @NotNull
    private String country;

    public Location() {
    }

    public Location(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Location(Double lat, Double lng, String name, String state, String country) {
        this.lat = lat;
        this.lng = lng;
        this.city = name;
        this.state = state;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
