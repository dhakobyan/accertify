package com.smartit.accertify.domain;

public interface Constants {

    String USA = "US";
    String Tokyo_Japan = "Tokyo, Japan";
    String Sydney_Australia = "Sydney, Australia";
    String Riyadh_Saudi_Arabia = "Riyadh, Saudi Arabia";
    String Zurich_Switzerland = "Zurich, Switzerland";
    String Reykjavik_Iceland = "Reykjavik, Iceland";
    String Mexico_City_Mexico = "Mexico City, Mexico";
    String Lima_Peru = "Lima, Peru";
    String Westminster_CA = "Westminster, CA";

}
