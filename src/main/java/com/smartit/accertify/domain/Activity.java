package com.smartit.accertify.domain;

public class Activity {
    private String location;
    private boolean within500miles;
    private Double miles;

    public Activity(String location, Double miles) {
        this.location = location;
        this.within500miles = 500 > miles;
        this.miles = miles;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isWithin500miles() {
        return within500miles;
    }

    public void setWithin500miles(boolean within500miles) {
        this.within500miles = within500miles;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }
}
