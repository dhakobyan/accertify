package com.smartit.accertify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccertifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccertifyApplication.class, args);
	}

}
