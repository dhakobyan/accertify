package com.smartit.accertify.controller;

import com.smartit.accertify.domain.Activity;
import com.smartit.accertify.domain.Location;
import com.smartit.accertify.exception.DataNotFoundException;
import com.smartit.accertify.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/")
public class LocationController {

    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService service) {
        this.locationService = service;
    }

    @GetMapping(value = "/{latitude}/{longitude}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Location getGeoLocation(@PathVariable Double latitude, @PathVariable Double longitude) {
        Location location = locationService.getData(latitude, longitude);
        if (location == null) {
            throw new DataNotFoundException("GeoLocation");
        }
        return location;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Location> getGeoLocation() {
        return locationService.getAllDataSets();
    }

    @PostMapping(headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean addGeoLocation(@RequestParam("latitude")  Double latitude, @RequestParam("longitude") Double longitude) {
        if (!locationService.locationExists(latitude, longitude)) {
            locationService.addData(latitude, longitude);
            return false;
        }
        return true;
    }

    @PostMapping(value = "/validateActivityLocation", headers = "Accept=application/json",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?>  validateActivityLocation(@RequestBody final Location location) {
        if (locationService.isWithinUSA(location)) {
            return new ResponseEntity<>("{ \"withinUSA\": " + true + "}", HttpStatus.OK);
        }
        List<Activity> activities = locationService.fraudGeoCharacteristics(location);
        return new ResponseEntity<>(activities, HttpStatus.OK);
    }

}
