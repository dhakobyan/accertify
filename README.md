# Getting Started

## Prerequirements
* Java 8
* MySql 5 or above
* Maven 3.x

### How to prepare for executing the program.

### Prepare the database
Open mysql client and create new database `accertify`. Open the application.yml file and set the jdbc `username` and `password`. Make sure application can connect to the database.

### Prepare the database - Option 1

#### Generating the geolocation and loading into the database
Retrieve the reverse-geocoder from the url extract the archive with the highest version and copy the CSV file into the `data` folder.

```
https://github.com/thampiman/reverse-geocoder/tree/master/dist
```

If necessary to populate the database from the CSV open the java class LocationRunner and set `doNotPopulateDB` = false.
The application will read the CSV file from the data folder and import into the database.
Start the application and wait until db is populated.
Once ready set `doNotPopulateDB` = true.

### Prepare the database - Option 2
1. execute SQL `Dump20210606-struct.sql` to create the database table.
2. execute SQL `Dump20210606-data.sql` to import data into the database table.

### Build and run the application
Build the project using 
```
mvn clean package
```

To run from the terminal with maven
```
mvn spring-boot:run
```

To run from the terminal as a java project
```
java -jar target/accertify-0.0.1-SNAPSHOT.jar
```

### Testing Guides using CURL
* Test using curl command with coordinates of Tokyo, Japan 
```
curl --location --request POST 'http://localhost:8080/validateActivity' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "lat": 35.89767,
    "lng": 140.49943
    }'
```
* Response
```
[
    {
        "location": "Tokyo, Japan",
        "within500miles": true,
        "miles": 40.67887110286514
    },
    {
        "location": "Sydney, Australia",
        "within500miles": false,
        "miles": 4869.625256622358
    },
    {
        "location": "Riyadh, Saudi Arabia",
        "within500miles": false,
        "miles": 5431.545682626572
    },
    {
        "location": "Zurich, Switzerland",
        "within500miles": false,
        "miles": 5962.210602996111
    },
    {
        "location": "Reykjavik, Iceland",
        "within500miles": false,
        "miles": 5458.511262108811
    },
    {
        "location": "Mexico City, Mexico",
        "within500miles": false,
        "miles": 6979.6874079101
    },
    {
        "location": "Lima, Peru",
        "within500miles": false,
        "miles": 9578.864817218171
    }
]
```


* Test using curl command with coordinates of Irvine, USA
```
curl --location --request POST 'http://localhost:8080/validateActivity' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "lat": 33.66946,
    "lng": -117.82311
    }'
```
Response
```
{
    "withinUSA": true
}
```